package MasterObjectOrientedDesign.LiskovSubstitution;

public class SchoolStaff {
	private void makeAnnouncements() {
		System.out.println("made attendence ....");
	}
	
	private void takeAttendence() {
		System.out.println("took attendence ....");
	}
	
	private void collectPaperWoek() {
		System.out.println("collected paperwork ....");
	}
	
	private void conductHallwayDuties() {
		System.out.println("conduct hallway duties ...");
	}
	
	public void performOtherResposibilities() {
		makeAnnouncements();
		takeAttendence();
		collectPaperWoek();
		conductHallwayDuties();
		performOtherResposibilities();
	}
}
