package MasterObjectOrientedDesign.InterfaceSegregation;

public interface Accounting {

	public void prepareInvoice(); 

	public void chargeCustomer();

}