package MasterObjectOrientedDesign.ObjectDesignPrinciples.domainDao;

import java.sql.SQLException;

import MasterObjectOrientedDesign.ObjectDesignPrinciples.domain.Employee;

public class EmployeeDAO {
	
	public void saveEmployee(Employee employee) throws SQLException {
//		DatabaseConnectionManager connectionMenager = DatabaseConnectionManager.getMenagerInstance();
//		connectionMenager.connect();
//		connectionMenager.getConnectionObject().prepareStatement("insert into Employee tbl");
//		connectionMenager.disconnet();
		System.out.println("save employee to the database " + employee);
		
	}
	
	public void deleteEmployee(Employee employee) {
		System.out.println("delete employee from the database " + employee);
	}
}
