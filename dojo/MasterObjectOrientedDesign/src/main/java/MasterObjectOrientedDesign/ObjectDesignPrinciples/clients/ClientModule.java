package MasterObjectOrientedDesign.ObjectDesignPrinciples.clients;

import java.sql.SQLException;

import MasterObjectOrientedDesign.ObjectDesignPrinciples.domain.Employee;
import MasterObjectOrientedDesign.ObjectDesignPrinciples.domainDao.EmployeeDAO;
import MasterObjectOrientedDesign.ObjectDesignPrinciples.reporting.EmployeeReportFormatter;
import MasterObjectOrientedDesign.ObjectDesignPrinciples.reporting.FormatType;

public class ClientModule {
	public static void main(String[] args) throws SQLException {
		Employee tom = new Employee(12, "Tom", "accounting", true);
		ClientModule.hireNewEmployee(tom);
		printEmployeeReport(tom,FormatType.XML);
	}
	
	public static void hireNewEmployee(Employee employee) throws SQLException {
		EmployeeDAO employeeDao = new EmployeeDAO();
		employeeDao.saveEmployee(employee);
	}
	
	public static void terminateEmployee(Employee employee){
		EmployeeDAO employeeDao = new EmployeeDAO();
		employeeDao.deleteEmployee(employee);
	}
	
	public static void printEmployeeReport(Employee employee,FormatType formatType){
		EmployeeReportFormatter formatter = new EmployeeReportFormatter(employee, formatType);
		System.out.println(formatter.getFormattedEmploye());
		
	}
}
