package MasterObjectOrientedDesign.ObjectDesignPrinciples.reporting;

import MasterObjectOrientedDesign.ObjectDesignPrinciples.domain.Employee;

public class EmployeeReportFormatter extends ReportFormatter{

	public EmployeeReportFormatter(Employee employee, FormatType formatType) {
		super(employee, formatType);
		
	}
		public String getFormattedEmploye() {
			return getFormattedValue();
		}
}
