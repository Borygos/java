package MasterObjectOrientedDesign.ObjectDesignPrinciples.database;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class DatabaseConnectionManager {
	//singelton pattern
	private Connection conn;
	
	private static DatabaseConnectionManager connectionInstance = new DatabaseConnectionManager();
	
	private DatabaseConnectionManager(){
		
	}
	
	public static DatabaseConnectionManager getMenagerInstance() {
		return connectionInstance;
	}
	
	public void connect() throws SQLException{
		//processing specific to database connection details....
		conn = DriverManager.getConnection("Some/Database/URL");
		System.out.println("Establish Database Connection ....");
	}
	
	public Connection getConnectionObject() {
		return conn;
	}
	
	public void disconnet() throws SQLException{
		conn.close();
		System.out.println("Disconnected from Databasee ....");
	}
}
