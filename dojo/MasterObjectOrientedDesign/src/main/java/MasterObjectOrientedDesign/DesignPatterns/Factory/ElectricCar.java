package MasterObjectOrientedDesign.DesignPatterns.Factory;

public class ElectricCar implements Vehicle{

	public void startEngine() {
		System.out.println("pushed button started electric car's engine");
		
	}

}
