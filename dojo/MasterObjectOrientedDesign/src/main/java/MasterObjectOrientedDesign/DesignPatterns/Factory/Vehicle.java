package MasterObjectOrientedDesign.DesignPatterns.Factory;

public interface Vehicle {
	public void startEngine();
}
