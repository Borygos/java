package MasterObjectOrientedDesign.DesignPatterns.Observer.clients;

import java.util.Date;

import MasterObjectOrientedDesign.DesignPatterns.Observer.domain.Employee;
import MasterObjectOrientedDesign.DesignPatterns.Observer.observers.HRDepartment;
import MasterObjectOrientedDesign.DesignPatterns.Observer.observers.IObserver;
import MasterObjectOrientedDesign.DesignPatterns.Observer.observers.PayrollDepartment;
import MasterObjectOrientedDesign.DesignPatterns.Observer.subjects.EmployeeMenagmentSystem;

public class App {

	public static void main(String[] args) {
		IObserver payroll = new PayrollDepartment();
		IObserver hrSystem = new HRDepartment();
		
		EmployeeMenagmentSystem ems = new EmployeeMenagmentSystem();
		
		ems.registerObserver(payroll);
		ems.registerObserver(hrSystem);
		
		Employee bob = new Employee("Bob", new Date(), 35000, true);
		ems.hireNewEmployee(bob);
//		ems.modifyEmployeeName(5,"Intiaz");
		

	}

}
