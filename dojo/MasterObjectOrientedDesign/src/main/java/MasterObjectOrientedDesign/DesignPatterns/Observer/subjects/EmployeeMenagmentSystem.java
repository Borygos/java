package MasterObjectOrientedDesign.DesignPatterns.Observer.subjects;


import java.util.ArrayList;
import java.util.List;

import MasterObjectOrientedDesign.DesignPatterns.Observer.domain.Employee;
import MasterObjectOrientedDesign.DesignPatterns.Observer.domain.EmployeeDao;
import MasterObjectOrientedDesign.DesignPatterns.Observer.observers.IObserver;





public class EmployeeMenagmentSystem implements ISubject{
	
	private List<IObserver> observers;
	private List<Employee> employees;
	
	private EmployeeDao employeeDao;
	
	private Employee employee;
	private String msg;
	
	public EmployeeMenagmentSystem() {
		observers = new ArrayList<IObserver>();
		employeeDao = new EmployeeDao();
		employees = employeeDao.generateEmployees();
		
	}
	public void registerObserver(IObserver o) {
		observers.add(o);
		
	}

	public void removeObserver(IObserver o) {
		observers.remove(o);
		
	}
	public void notifyObservers() {
		for(IObserver departments : observers) {
			departments.callMe(employee, msg);
		}
		
	}
	
	public void hireNewEmployee(Employee emp) {
		employee = emp;
		msg = "New Hire";
		employees.add(emp);
		notifyObservers();
	}
	public void nodifyEmployeeName(int id, String newName) {
		boolean notify = false;
		
		for (Employee emp : employees) {
			if (id == emp.employeeID) {
				emp.setName(newName);
				this.employee = emp;
				this.msg = "Employee name has been modified";
			}
		}
		if(notify) {
			notifyObservers();
		}
	}

}
