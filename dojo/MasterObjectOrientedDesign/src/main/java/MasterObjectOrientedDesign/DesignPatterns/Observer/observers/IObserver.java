package MasterObjectOrientedDesign.DesignPatterns.Observer.observers;

import MasterObjectOrientedDesign.DesignPatterns.Observer.domain.Employee;

public interface IObserver {
	public void callMe(Employee emp, String msg);
}
