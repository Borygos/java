package MasterObjectOrientedDesign.DesignPatterns.Bulider;

public class User {
	private String userName;
	private String emailAdress;
	
	
	private String firstName;
	private String lastName;
	private int phoneNumber;
	private String address;
	
	private User(Bulider bulider) {
		this.userName = bulider.userName;
		this.emailAdress = bulider.emailAdress;
		this.firstName = bulider.firstName;
		this.phoneNumber = bulider.phoneNumber;
		this.address = bulider.address;
	}
	
	@Override
	public String toString() {
		return "User [userName=" + userName + ", emailAdress=" + emailAdress + ", firstName=" + firstName
				+ ", lastName=" + lastName + ", phoneNumber=" + phoneNumber + ", address=" + address + "]";
	}
	
	public static class Bulider{
		private String userName;
		private String emailAdress;
		
		
		private String firstName;
		private String lastName;
		private int phoneNumber;
		private String address;
		 
		
		public Bulider(String userName, String email) {
			this.userName = userName;
			this.emailAdress = email;
		}
		
		public Bulider firstName(String value) {
			this.firstName = value;
			return this;
		}
		
		public Bulider lastName(String value) {
			this.lastName = value;
			return this;
		}
		
		public User bulid() {
			return new User(this);
		}
	}
}

