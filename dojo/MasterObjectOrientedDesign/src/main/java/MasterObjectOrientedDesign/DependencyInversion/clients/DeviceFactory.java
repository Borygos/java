package MasterObjectOrientedDesign.DependencyInversion.clients;

import MasterObjectOrientedDesign.DependencyInversion.processes.GeneralManufacturingProcess;
import MasterObjectOrientedDesign.DependencyInversion.processes.SmartphoneManufacturingProcess;

public class DeviceFactory {
	public static void main(String[] args) {
		GeneralManufacturingProcess manfacturer = new SmartphoneManufacturingProcess("Iphone");
		manfacturer.lanuchProcess();
	}
}
