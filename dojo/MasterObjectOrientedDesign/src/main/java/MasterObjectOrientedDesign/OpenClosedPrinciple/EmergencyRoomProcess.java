package MasterObjectOrientedDesign.OpenClosedPrinciple;

public class EmergencyRoomProcess {

	public static void main(String[] args) {
		
		HospitalMenagment ERDirector = new HospitalMenagment();
		Employee peggy = new Nurse(1,"Peggy","emergency",true);
		
			
		ERDirector.callUpon(peggy);
		
		Employee tom = new Doctor(12, "Tom", "emergency", true);
		
		ERDirector.callUpon(tom);
			
	}

}
