package MasterObjectOrientedDesign.OpenClosedPrinciple;

public class Doctor extends Employee {

	public Doctor(int id, String name, String departament, boolean working) {
		super(id, name, departament, working);
		
	}
	
	private void prescribeMedicine() {
		System.out.println("Prescribe Medicine ....");
	}
	
	private void diagnosePatients() {
		System.out.println("Diagnosing Patients ....");
	}

	@Override
	public void performDuties() {
		prescribeMedicine();
		diagnosePatients();
	
	}

}
