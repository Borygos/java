package MasterObjectOrientedDesign.OpenClosedPrinciple;

 public abstract class Employee {
	
	private int id;
	private String name;
	private String departament;
	private boolean working;
	
	public Employee(int id, String name, String departament, boolean working) {		
		this.id = id;
		this.name = name;
		this.departament = departament;
		this.working = working;
	}

		public abstract void performDuties();
	@Override
	public String toString() {
		return "Employee [id=" + id + ", name=" + name + ", departament=" + departament + ", working=" + working + "]";
	}
	
	
	
}
