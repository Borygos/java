package MasterObjectOrientedDesign.OpenClosedPrinciple;

public class Nurse extends Employee{

	public Nurse(int id, String name, String departament, boolean working) {
		super(id, name, departament, working);
		System.out.println("Nurse in action ....");
	}

	
	private void checkVitalSigns() {
		System.out.println("Checking Vitals ....");
	}
	
	private void drawBlood() {
		System.out.println("Drawing Blood ....");
	}
	
	private void cleanPatientArea() {
		System.out.println("Cleaning Patient Area ....");
	}
	@Override
	public void performDuties() {		
		checkVitalSigns();
		drawBlood();
		cleanPatientArea();
	}

}
