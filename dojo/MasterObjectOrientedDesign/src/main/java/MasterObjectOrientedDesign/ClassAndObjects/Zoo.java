package MasterObjectOrientedDesign.ClassAndObjects;

public class Zoo {

	public static void main(String[] args) {
		Animal sparrow1 = new Sparrow("M",4, 4);
		Animal fish1 = new Fish("M", 4, 4);
		
		Flyable flyingBird = new Sparrow("M", 4, 4);
		flyingBird.fly();
		
		moveAnimal(sparrow1);
		moveAnimal(fish1);
	}

	
	public static void moveAnimal(Animal animal) {
		animal.move();
	}
}
