package MasterObjectOrientedDesign.ClassAndObjects;

public class Bird extends Animal implements Flyable{
	public Bird(String gender, int age, int weightInLbs) {
		super(gender, age, weightInLbs);
		
	}

	public void fly() {
		System.out.println("Flying....");
		
	}

	@Override
	public void move() {
		System.out.println("Flapping wings ....");
		
	}



}
