package MasterObjectOrientedDesign.ClassAndObjects;

public class Sparrow extends Bird implements Flyable{

	public Sparrow(String gender, int age, int weightInLbs) {
		super(gender, age, weightInLbs);
		
	}
	public void fly() {
		System.out.println("Sparrow flying high ....");
	}
}
