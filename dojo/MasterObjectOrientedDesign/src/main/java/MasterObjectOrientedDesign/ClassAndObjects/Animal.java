package MasterObjectOrientedDesign.ClassAndObjects;

public abstract class Animal {
	String gender;
	int age;
	int weightInLbs;
	
	public Animal(String gender, int age, int weightInLbs) {	
		this.gender = gender;
		this.age = age;
		this.weightInLbs = weightInLbs;
	}
	public void eat() {
		System.out.println("Eating ....");
	}
	 public void slep() {
		 System.out.println("Sleeping ....");
	 }
	 
	public abstract void move();
}
