package OOP;

public class HealthyBurger extends Hamburger{

	private String healthyExtra1Name;
	private double healtthyExtra1Price;
	
	private String healthyExtra2Name;
	private double healtthyExtra2Price;
	
	public HealthyBurger(String meat, double price) {
		super("Health",meat,price,"Brown rye");
	}
	
	public void addHealthyAdditin1(String name,double price) {
		this.healthyExtra1Name = name;
		this.healtthyExtra1Price = price;
	}
	
	public void addHealthyAdditin2(String name,double price) {
		this.healthyExtra2Name = name;
		this.healtthyExtra2Price = price;
	}

	@Override
	public double itemizeHamburger() {
		double hamburgerPrice = super.itemizeHamburger();
		if(this.healthyExtra1Name != null) {
			hamburgerPrice += this.healtthyExtra1Price;
			System.out.println("Added " + this.healthyExtra1Name + " for an extra " + this.healtthyExtra1Price);
		}
		if(this.healthyExtra2Name != null) {
			hamburgerPrice += this.healtthyExtra2Price;
			System.out.println("Added " + this.healthyExtra2Name + " for an extra " + this.healtthyExtra2Price);
		}
		return hamburgerPrice;
	}
	
}
